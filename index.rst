.. FFTA documentation master file, created by
   sphinx-quickstart on Thu Apr  2 17:20:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FFTA's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
.. py:function:: enumerate(sequence[, start=0])


Indices and tables
==================
.. autofunction:: ffta.analyze_pixel.analyze_pixel

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Test Menu
==================
